# Script to uninstall apps from an Atlassin instance

import requests
import getpass
import json
import re


# glabal variables

basic_get = "/rest/plugins/1.0/"
#delete_app = "/rest/plugins/1.0/{0}-key".format(app_key)

def detail_grabber():
    global instance_url
    global user_name
    global user_password
    instance_url_raw = str(raw_input("URL of your instance: \n"))
    #remove the trailing slash if the user added it
    instance_url = instance_url_raw.rstrip("/")
    user_name = str(raw_input("Username: (User must have admin permissions) \n"))
    user_password = getpass.getpass("Admin password: \n")
    return instance_app_status(instance_url, user_name, user_password)


def instance_app_status(instance_url, user_name, user_password):
    retrieve = requests.get(instance_url + basic_get, auth=(user_name, user_password))
    json_app_list = json.loads(retrieve.text)
    access_token_checker(json_app_list)
    return app_sorter(json_app_list)


def app_sorter(apps):
    installed_apps = []
    for i in apps['plugins']:
        if i['userInstalled']:
            installed_apps.append(i['key'])
    return atlassian_app_check(installed_apps)


def decision(installed_apps):
    user_response = str(raw_input(("There are {0} apps installed on {1}, remove these? \n y/n \n".format(len(installed_apps), instance_url))))
    if user_response == "y":
        delete_app(installed_apps)
    else:
        print("Aborting")

def delete_app(installed_apps):
    for i in installed_apps:
        a = requests.delete(instance_url + "/rest/plugins/1.0/{}-key".format(i), auth=(user_name, user_password))
        if a.status_code == 204:
            print("Uninstalled {} successfully".format(i))
        else:
            print("Status code was {0}, {1} may not have been uninstalled".format(a.status_code, i))


def access_token_checker(apps):
    for i in apps['plugins']:
        if i['userInstalled']:
            if 'delete' not in i['links']:
                print("Removing access token for {}".format(i['key']))
                b = requests.delete(instance_url + "/rest/plugins/1.0/license-tokens/{}-key".format(i['key']), auth=(user_name, user_password))
                if b.status_code != 204:
                    print("The Access Token was not removed, Status Code is {}".format(b.status_code))


def atlassian_app_check(apps):
    list2 = []
    filtered_apps = []
    pattern = r"^com.atlassian."
    pattern2 = r"greenhopper"
    for i in apps:
        if not re.match(pattern, i):
            list2.append(i)
    for i in list2:
        if not re.search(pattern2, i):
            filtered_apps.append(i)
    return decision(filtered_apps)



detail_grabber()
